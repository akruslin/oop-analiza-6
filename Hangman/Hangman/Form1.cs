﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hangman
{
    public partial class Hangman : Form
    {
        string pogodi;
        List<string> rijeci = new List<string>();
        string path = "C:/Users/krusl.ANTONIO/source/repos/Hangman/Hangman/TextFile1.txt";
        //txt file se nalazi u solutionu
        public Hangman()
        {
            InitializeComponent();
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    rijeci.Add(line);
                }
            }
            button1.Enabled = false;
            button2.Enabled = false;

        }
            
        int br;
        int life;
        string guess;


        private void button1_Click(object sender, EventArgs e)
        {
            string cletter = textBox1.Text.ToUpper();
            if (pogodi.Contains(cletter))
            {
                for (int i = 0; i < pogodi.Length; i++)
                {
                    if(pogodi[i]==cletter[0])
                    {
                        label1.Text = label1.Text.Remove(i, 1);
                        label1.Text = label1.Text.Insert(i,cletter.ToString());
                        br++;
                    }
                }
                if (br==pogodi.Length)
                {
                    label1.Text = pogodi;
                    MessageBox.Show("BRAVO, Tražena riječ bila je " + pogodi);
                    button1.Enabled = false;
                    button2.Enabled = false;
                }
            }
            else
            {
                
                life--;
                label2.Text = life.ToString();
                if (life == 0)
                {
                    MessageBox.Show("Kraj igre, tražena riječ bila je " + pogodi);
                    button1.Enabled = false;
                    button2.Enabled = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            guess = textBox2.Text.ToUpper();
            if (guess == pogodi)
            {
            label1.Text = pogodi;
            MessageBox.Show("BRAVO, Tražena riječ bila je "+pogodi);
            button1.Enabled = false;
            button2.Enabled = false;
            }
            else
            {
               
                life--;
                label2.Text = life.ToString();
                if (life == 0)
                {
                    MessageBox.Show("Kraj igre, tražena riječ bila je " + pogodi);
                    button1.Enabled = false;
                    button2.Enabled = false;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            pogodi = rijeci[random.Next(0, rijeci.Count)];
            label1.Text = "";
            for (int i = 0; i < pogodi.Length; i++)
            {
                label1.Text = label1.Text.Insert(i, "*");
            }
            life = 5;
            br = 0;
            label2.Text = life.ToString();
            button1.Enabled = true;
            button2.Enabled = true;
        }
    }
}
