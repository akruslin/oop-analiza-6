﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KalkulatorPro
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        decimal num1;

        decimal num2;

        decimal tnum;

        string operation;

        private void input(string a)
        {
            if (textBox1.Text == "0")

                textBox1.Text = a;

            else

                textBox1.Text += a;
        }

        private void bt_0_Click(object sender, EventArgs e)
        {
            input("0");
        }

        private void bt_1_Click(object sender, EventArgs e)
        {
            input("1");
        }

        private void bt_2_Click(object sender, EventArgs e)
        {
            input("2");
        }

        private void bt_3_Click(object sender, EventArgs e)
        {
            input("3");
        }

        private void bt_4_Click(object sender, EventArgs e)
        {
            input("4");
        }

        private void bt_5_Click(object sender, EventArgs e)
        {
            input("5");
        }

        private void bt_6_Click(object sender, EventArgs e)
        {
            input("6");
        }

        private void bt_7_Click(object sender, EventArgs e)
        {
            input("7");
        }

        private void bt_8_Click(object sender, EventArgs e)
        {
            input("8");
        }

        private void bt_9_Click(object sender, EventArgs e)
        {
            input("9");
        }

        private void bt_sum_Click(object sender, EventArgs e)
        {
            num1 = decimal.Parse(textBox1.Text);
            operation = "+";
            textBox1.Text = "0";
            string x = num1 + "+";
            label1.Text = x;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string x;
            num2 = decimal.Parse(textBox1.Text);
            switch (operation)
            {
                case "+":
                    textBox1.Text = (num1 + num2).ToString();
                    x = num1.ToString()+"+"+ num2.ToString()+"=";
                    label1.Text = x;
                    break;
                case "-":
                    textBox1.Text = (num1 - num2).ToString();
                    x = num1.ToString() + "-" + num2.ToString() + "=";
                    label1.Text = x;
                    break;
                case "*":
                    textBox1.Text = (num1 * num2).ToString();
                    x = num1.ToString() + "*" + num2.ToString() + "=";
                    label1.Text = x;
                    break;
                case "/":
                    textBox1.Text = (num1 / num2).ToString();
                    x = num1.ToString() + "/" + num2.ToString() + "=";
                    label1.Text = x;
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            num1 = decimal.Parse(textBox1.Text);
            operation = "-";
            textBox1.Text = "0";
        }

        private void bt_mult_Click(object sender, EventArgs e)
        {
            num1 = decimal.Parse(textBox1.Text);
            operation = "*";
            textBox1.Text = "0";
        }

        private void bt_div_Click(object sender, EventArgs e)
        {
            num1 = decimal.Parse(textBox1.Text);
            operation = "/";
            textBox1.Text = "0";
        }

        private void bt_sin_Click(object sender, EventArgs e)
        {
            string x = "Sin(" + textBox1.Text + ")";
            label1.Text = x;
            textBox1.Text= (Math.Sin(double.Parse(textBox1.Text))).ToString();
            
        }

        private void bt_cos_Click(object sender, EventArgs e)
        {
            string x = "Cos(" + textBox1.Text + ")";
            label1.Text = x;
            textBox1.Text = (Math.Cos(double.Parse(textBox1.Text))).ToString();
        }

        private void bt_tan_Click(object sender, EventArgs e)
        {
            string x = "Tan(" + textBox1.Text + ")";
            label1.Text = x;
            textBox1.Text = (Math.Tan(double.Parse(textBox1.Text))).ToString();
        }

        private void bt_pow2_Click(object sender, EventArgs e)
        {
            double a=0;
            a = double.Parse(textBox1.Text);
            string x = textBox1.Text + "^2";
            label1.Text = x;
            textBox1.Text = (a * a).ToString();
        }

        private void bt_sqrt_Click(object sender, EventArgs e)
        {
            string x = "Sqrt(" + textBox1.Text + ")";
            label1.Text = x;
            textBox1.Text = (Math.Sqrt(double.Parse(textBox1.Text))).ToString();
        }

        private void bt_clear_Click(object sender, EventArgs e)
        {
            num1 = 0;
            num2 = 0;
            operation = "";
            textBox1.Text="0";
            label1.Text = "0";
        }
    }
}
