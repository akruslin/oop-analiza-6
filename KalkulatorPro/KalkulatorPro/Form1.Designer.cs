﻿namespace KalkulatorPro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_7 = new System.Windows.Forms.Button();
            this.bt_4 = new System.Windows.Forms.Button();
            this.bt_1 = new System.Windows.Forms.Button();
            this.bt_8 = new System.Windows.Forms.Button();
            this.bt_5 = new System.Windows.Forms.Button();
            this.bt_2 = new System.Windows.Forms.Button();
            this.bt_9 = new System.Windows.Forms.Button();
            this.bt_6 = new System.Windows.Forms.Button();
            this.bt_3 = new System.Windows.Forms.Button();
            this.bt_0 = new System.Windows.Forms.Button();
            this.bt_sum = new System.Windows.Forms.Button();
            this.bt_sub = new System.Windows.Forms.Button();
            this.bt_div = new System.Windows.Forms.Button();
            this.bt_mult = new System.Windows.Forms.Button();
            this.bt_eq = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.bt_sin = new System.Windows.Forms.Button();
            this.bt_cos = new System.Windows.Forms.Button();
            this.bt_tan = new System.Windows.Forms.Button();
            this.bt_pow2 = new System.Windows.Forms.Button();
            this.bt_sqrt = new System.Windows.Forms.Button();
            this.bt_clear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bt_7
            // 
            this.bt_7.Location = new System.Drawing.Point(29, 112);
            this.bt_7.Name = "bt_7";
            this.bt_7.Size = new System.Drawing.Size(75, 23);
            this.bt_7.TabIndex = 0;
            this.bt_7.Text = "7";
            this.bt_7.UseVisualStyleBackColor = true;
            this.bt_7.Click += new System.EventHandler(this.bt_7_Click);
            // 
            // bt_4
            // 
            this.bt_4.Location = new System.Drawing.Point(29, 141);
            this.bt_4.Name = "bt_4";
            this.bt_4.Size = new System.Drawing.Size(75, 23);
            this.bt_4.TabIndex = 1;
            this.bt_4.Text = "4";
            this.bt_4.UseVisualStyleBackColor = true;
            this.bt_4.Click += new System.EventHandler(this.bt_4_Click);
            // 
            // bt_1
            // 
            this.bt_1.Location = new System.Drawing.Point(29, 170);
            this.bt_1.Name = "bt_1";
            this.bt_1.Size = new System.Drawing.Size(75, 23);
            this.bt_1.TabIndex = 2;
            this.bt_1.Text = "1";
            this.bt_1.UseVisualStyleBackColor = true;
            this.bt_1.Click += new System.EventHandler(this.bt_1_Click);
            // 
            // bt_8
            // 
            this.bt_8.Location = new System.Drawing.Point(110, 112);
            this.bt_8.Name = "bt_8";
            this.bt_8.Size = new System.Drawing.Size(75, 23);
            this.bt_8.TabIndex = 3;
            this.bt_8.Text = "8";
            this.bt_8.UseVisualStyleBackColor = true;
            this.bt_8.Click += new System.EventHandler(this.bt_8_Click);
            // 
            // bt_5
            // 
            this.bt_5.Location = new System.Drawing.Point(110, 141);
            this.bt_5.Name = "bt_5";
            this.bt_5.Size = new System.Drawing.Size(75, 23);
            this.bt_5.TabIndex = 4;
            this.bt_5.Text = "5";
            this.bt_5.UseVisualStyleBackColor = true;
            this.bt_5.Click += new System.EventHandler(this.bt_5_Click);
            // 
            // bt_2
            // 
            this.bt_2.Location = new System.Drawing.Point(110, 170);
            this.bt_2.Name = "bt_2";
            this.bt_2.Size = new System.Drawing.Size(75, 23);
            this.bt_2.TabIndex = 5;
            this.bt_2.Text = "2";
            this.bt_2.UseVisualStyleBackColor = true;
            this.bt_2.Click += new System.EventHandler(this.bt_2_Click);
            // 
            // bt_9
            // 
            this.bt_9.Location = new System.Drawing.Point(191, 112);
            this.bt_9.Name = "bt_9";
            this.bt_9.Size = new System.Drawing.Size(75, 23);
            this.bt_9.TabIndex = 6;
            this.bt_9.Text = "9";
            this.bt_9.UseVisualStyleBackColor = true;
            this.bt_9.Click += new System.EventHandler(this.bt_9_Click);
            // 
            // bt_6
            // 
            this.bt_6.Location = new System.Drawing.Point(191, 141);
            this.bt_6.Name = "bt_6";
            this.bt_6.Size = new System.Drawing.Size(75, 23);
            this.bt_6.TabIndex = 7;
            this.bt_6.Text = "6";
            this.bt_6.UseVisualStyleBackColor = true;
            this.bt_6.Click += new System.EventHandler(this.bt_6_Click);
            // 
            // bt_3
            // 
            this.bt_3.Location = new System.Drawing.Point(191, 170);
            this.bt_3.Name = "bt_3";
            this.bt_3.Size = new System.Drawing.Size(75, 23);
            this.bt_3.TabIndex = 8;
            this.bt_3.Text = "3";
            this.bt_3.UseVisualStyleBackColor = true;
            this.bt_3.Click += new System.EventHandler(this.bt_3_Click);
            // 
            // bt_0
            // 
            this.bt_0.Location = new System.Drawing.Point(110, 199);
            this.bt_0.Name = "bt_0";
            this.bt_0.Size = new System.Drawing.Size(75, 23);
            this.bt_0.TabIndex = 9;
            this.bt_0.Text = "0";
            this.bt_0.UseVisualStyleBackColor = true;
            this.bt_0.Click += new System.EventHandler(this.bt_0_Click);
            // 
            // bt_sum
            // 
            this.bt_sum.Location = new System.Drawing.Point(309, 109);
            this.bt_sum.Name = "bt_sum";
            this.bt_sum.Size = new System.Drawing.Size(75, 23);
            this.bt_sum.TabIndex = 10;
            this.bt_sum.Text = "+";
            this.bt_sum.UseVisualStyleBackColor = true;
            this.bt_sum.Click += new System.EventHandler(this.bt_sum_Click);
            // 
            // bt_sub
            // 
            this.bt_sub.Location = new System.Drawing.Point(309, 138);
            this.bt_sub.Name = "bt_sub";
            this.bt_sub.Size = new System.Drawing.Size(75, 23);
            this.bt_sub.TabIndex = 11;
            this.bt_sub.Text = "-";
            this.bt_sub.UseVisualStyleBackColor = true;
            this.bt_sub.Click += new System.EventHandler(this.button1_Click);
            // 
            // bt_div
            // 
            this.bt_div.Location = new System.Drawing.Point(309, 196);
            this.bt_div.Name = "bt_div";
            this.bt_div.Size = new System.Drawing.Size(75, 23);
            this.bt_div.TabIndex = 12;
            this.bt_div.Text = "/";
            this.bt_div.UseVisualStyleBackColor = true;
            this.bt_div.Click += new System.EventHandler(this.bt_div_Click);
            // 
            // bt_mult
            // 
            this.bt_mult.Location = new System.Drawing.Point(309, 167);
            this.bt_mult.Name = "bt_mult";
            this.bt_mult.Size = new System.Drawing.Size(75, 23);
            this.bt_mult.TabIndex = 13;
            this.bt_mult.Text = "*";
            this.bt_mult.UseVisualStyleBackColor = true;
            this.bt_mult.Click += new System.EventHandler(this.bt_mult_Click);
            // 
            // bt_eq
            // 
            this.bt_eq.Location = new System.Drawing.Point(309, 225);
            this.bt_eq.Name = "bt_eq";
            this.bt_eq.Size = new System.Drawing.Size(75, 23);
            this.bt_eq.TabIndex = 14;
            this.bt_eq.Text = "=";
            this.bt_eq.UseVisualStyleBackColor = true;
            this.bt_eq.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(29, 47);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(237, 22);
            this.textBox1.TabIndex = 15;
            // 
            // bt_sin
            // 
            this.bt_sin.Location = new System.Drawing.Point(400, 109);
            this.bt_sin.Name = "bt_sin";
            this.bt_sin.Size = new System.Drawing.Size(75, 23);
            this.bt_sin.TabIndex = 16;
            this.bt_sin.Text = "Sin(x)";
            this.bt_sin.UseVisualStyleBackColor = true;
            this.bt_sin.Click += new System.EventHandler(this.bt_sin_Click);
            // 
            // bt_cos
            // 
            this.bt_cos.Location = new System.Drawing.Point(400, 138);
            this.bt_cos.Name = "bt_cos";
            this.bt_cos.Size = new System.Drawing.Size(75, 23);
            this.bt_cos.TabIndex = 17;
            this.bt_cos.Text = "Cos(x)";
            this.bt_cos.UseVisualStyleBackColor = true;
            this.bt_cos.Click += new System.EventHandler(this.bt_cos_Click);
            // 
            // bt_tan
            // 
            this.bt_tan.Location = new System.Drawing.Point(400, 167);
            this.bt_tan.Name = "bt_tan";
            this.bt_tan.Size = new System.Drawing.Size(75, 23);
            this.bt_tan.TabIndex = 18;
            this.bt_tan.Text = "Tan(x)";
            this.bt_tan.UseVisualStyleBackColor = true;
            this.bt_tan.Click += new System.EventHandler(this.bt_tan_Click);
            // 
            // bt_pow2
            // 
            this.bt_pow2.Location = new System.Drawing.Point(400, 199);
            this.bt_pow2.Name = "bt_pow2";
            this.bt_pow2.Size = new System.Drawing.Size(75, 23);
            this.bt_pow2.TabIndex = 19;
            this.bt_pow2.Text = "x^2";
            this.bt_pow2.UseVisualStyleBackColor = true;
            this.bt_pow2.Click += new System.EventHandler(this.bt_pow2_Click);
            // 
            // bt_sqrt
            // 
            this.bt_sqrt.Location = new System.Drawing.Point(400, 228);
            this.bt_sqrt.Name = "bt_sqrt";
            this.bt_sqrt.Size = new System.Drawing.Size(75, 23);
            this.bt_sqrt.TabIndex = 20;
            this.bt_sqrt.Text = "Sqrt";
            this.bt_sqrt.UseVisualStyleBackColor = true;
            this.bt_sqrt.Click += new System.EventHandler(this.bt_sqrt_Click);
            // 
            // bt_clear
            // 
            this.bt_clear.Location = new System.Drawing.Point(191, 199);
            this.bt_clear.Name = "bt_clear";
            this.bt_clear.Size = new System.Drawing.Size(75, 23);
            this.bt_clear.TabIndex = 21;
            this.bt_clear.Text = "C";
            this.bt_clear.UseVisualStyleBackColor = true;
            this.bt_clear.Click += new System.EventHandler(this.bt_clear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 259);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bt_clear);
            this.Controls.Add(this.bt_sqrt);
            this.Controls.Add(this.bt_pow2);
            this.Controls.Add(this.bt_tan);
            this.Controls.Add(this.bt_cos);
            this.Controls.Add(this.bt_sin);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.bt_eq);
            this.Controls.Add(this.bt_mult);
            this.Controls.Add(this.bt_div);
            this.Controls.Add(this.bt_sub);
            this.Controls.Add(this.bt_sum);
            this.Controls.Add(this.bt_0);
            this.Controls.Add(this.bt_3);
            this.Controls.Add(this.bt_6);
            this.Controls.Add(this.bt_9);
            this.Controls.Add(this.bt_2);
            this.Controls.Add(this.bt_5);
            this.Controls.Add(this.bt_8);
            this.Controls.Add(this.bt_1);
            this.Controls.Add(this.bt_4);
            this.Controls.Add(this.bt_7);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_7;
        private System.Windows.Forms.Button bt_4;
        private System.Windows.Forms.Button bt_1;
        private System.Windows.Forms.Button bt_8;
        private System.Windows.Forms.Button bt_5;
        private System.Windows.Forms.Button bt_2;
        private System.Windows.Forms.Button bt_9;
        private System.Windows.Forms.Button bt_6;
        private System.Windows.Forms.Button bt_3;
        private System.Windows.Forms.Button bt_0;
        private System.Windows.Forms.Button bt_sum;
        private System.Windows.Forms.Button bt_sub;
        private System.Windows.Forms.Button bt_div;
        private System.Windows.Forms.Button bt_mult;
        private System.Windows.Forms.Button bt_eq;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button bt_sin;
        private System.Windows.Forms.Button bt_cos;
        private System.Windows.Forms.Button bt_tan;
        private System.Windows.Forms.Button bt_pow2;
        private System.Windows.Forms.Button bt_sqrt;
        private System.Windows.Forms.Button bt_clear;
        private System.Windows.Forms.Label label1;
    }
}

